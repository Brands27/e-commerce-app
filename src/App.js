import React from 'react';
import Navbar from './components/Navbar';
import './App.css';
import { useState, useEffect } from 'react';
import Home from './components/pages/Home';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';
import { UserProvider } from './UserContext'
import Login from './components/pages/Login';
import Register from './components/pages/Register';
import Logout from './components/pages/Logout';

// import Services from './components/pages/Services';
// import Products from './components/pages/Products';
// import SignUp from './components/pages/SignUp';

function App() {
  const [user, setUser] = useState({
    // email: localStorage.getItem('email')
    id: null,
    isAdmin: null
  })

  // Function for clearing localStorage on Logout
  const unsetUser = () => {
    localStorage.clear();
  }

  // Used to check if the user information is properly stored upon login and the localStorage information is cleared upon logout
  useEffect(() => {
    console.log(user);
    console.log(localStorage);
  }, [user])

  return (
    <UserProvider value={{user, setUser, unsetUser}}>
      <Router>
        <Navbar />
        <Switch>
          <Route path='/' exact component = {Home} />
          <Route exact path="/login" component={Login} />
          <Route exact path="/sign-up" component={Register} />
          <Route exact path="/logout" component={Logout} />
        </Switch>
      </Router>
    </UserProvider>
    
  );
}

export default App;
